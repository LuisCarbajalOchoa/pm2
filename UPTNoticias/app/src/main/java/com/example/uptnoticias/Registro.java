package com.example.uptnoticias;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.uptnoticias.Api.Api;
import com.example.uptnoticias.Api.Servicios.ServicioPeticion;
import com.example.uptnoticias.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {
    EditText nombreUsuario = (EditText) findViewById(R.id.txtNombre);
    EditText password1 = (EditText) findViewById(R.id.txtContra);
    EditText password2 = (EditText) findViewById(R.id.txtContra2);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Button btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registro.this, MainActivity.class));
            }
        });


        Button btnRegistro = (Button) findViewById(R.id.btnRegistro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(nombreUsuario.getText().toString() == ""){
                    nombreUsuario.setSelectAllOnFocus(true);
                    nombreUsuario.requestFocus();
                    return;
                }
                if(password1.getText().toString() == ""){
                    password1.setSelectAllOnFocus(true);
                    password1.requestFocus();
                    return;
                }
                if(password2.getText().toString() == ""){
                    password2.setSelectAllOnFocus(true);
                    password2.requestFocus();
                    return;
                }
                if(!password1.getText().toString().equals(password2.getText().toString())){
                    Toast.makeText(Registro.this, "Las Contraseñas no Coinciden", Toast.LENGTH_SHORT).show();
                    return;
                }

                ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
                Call<Registro_Usuario> registrarCall = service.registrarUsuario(nombreUsuario.getText().toString(),password1.getText().toString());
                registrarCall.enqueue(new Callback<Registro_Usuario>() {
                    @Override
                    public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                        Registro_Usuario peticion = response.body();
                        if(response.body() == null){
                            Toast.makeText(Registro.this, "Ocurrio un Error, Intentalo mas tarde ", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            startActivity(new Intent(Registro.this, MainActivity.class));
                            Toast.makeText(Registro.this, "Datos Registrados", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(Registro.this, peticion.detalle, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                        Toast.makeText(Registro.this, "Error :(", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });
    }
}
